import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Hello from './components/Hello'
import About from './components/About'
import Param from './components/Param'
import paramdetails from './components/paramdetails'

const routes = [
    { path: '/', component: Hello },
    { path: '/about', component: About },
    { path: '/param', component: Param },
    { path: '/Paramdetails/:id', component: paramdetails, name: 'Paramdetails' }
]

const router = new VueRouter({
    routes,
    mode: 'history'
})

router.beforeEach((to, from, next) => {
    if(to.path == '/param') {
	if(localStorage.getItem('user')==undefined){
	    var user = prompt('please enter your username:');
	    var pass = prompt('please enter your password:');
	    console.log(user);
	    console.log(pass);
	    if (user == 'username' && pass == 'password'){
		localStorage.setItem('user', user);
		next();
	    } else {
		alert('wrong');
		return;
	    }
	}
    }
    next()
})

new Vue({
    el: '#app',
    template: '<App/>',
    components: { App },
    router
}).$mount('#app')
